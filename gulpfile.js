var gulp = require("gulp");
var sass = require("gulp-sass");
var useref = require("gulp-useref");
var gulpIf = require("gulp-if");
var uglify = require("gulp-uglify");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var cssnano = require("cssnano");
var image = require("gulp-image");
var cache = require("gulp-cache");
var del = require("del");
var inlinesource = require("gulp-inline-source");
var syntax_scss = require("postcss-scss");
var reporter = require("postcss-reporter");
var stylelint = require("stylelint");
var modifyCssUrls = require("gulp-modify-css-urls");
var htmlmin = require("gulp-htmlmin");
const eslint = require("gulp-eslint");
var babel = require("gulp-babel");
var base_folder = process.cwd();
gulp.task("sass", function () {
    return gulp.src(["app/scss/**/*.scss", "!app/scss/mixins.scss", "!app/scss/modules/*.scss"], {base: base_folder + "/app/scss/"})
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest("app/css"));
});

gulp.task("css-lint", function () {

    var processors = [
        stylelint(),
        // Pretty reporting config
        reporter({
            clearMessages: true,
            throwError: false
        })
    ];

    return gulp.src(
        ["app/scss/**/*.scss",
            "!app/assets/css/vendor/**/*.scss"], {base: base_folder}
    )
        .pipe(postcss(processors, {syntax: syntax_scss}));
});



gulp.task("lint", function () {
    // ESLint ignores files with "node_modules" paths.
    // So, it's best to have gulp ignore the directory as well.
    // Also, Be sure to return the stream from the task;
    // Otherwise, the task may end before the stream has finished.
    return gulp.src(["app/js/*.js", "!node_modules/**"], {base: base_folder})
    // eslint() attaches the lint output to the "eslint" property
    // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format());
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        //.pipe(eslint.failAfterError());
});

gulp.task("watch", function () {
    gulp.watch("app/scss/**/*.scss", gulp.series("css-lint", "sass"));
    gulp.watch("app/js/*.js", gulp.series("lint", "scripts"));
    // Other watchers
});
gulp.task("useref", function () {
    var plugins = [
        autoprefixer({browsers: ["last 2 version"]}),
        cssnano()
    ];
    return gulp.src("app/*.html", {base: base_folder + "/app"})
        .pipe(useref())
        .pipe(gulpIf("js/es5/*.js", uglify()))
        .pipe(gulpIf("*.css", postcss(plugins)))
        .pipe(gulp.dest("dist"));
});
gulp.task("images", function () {
    return gulp.src("app/images/**/*.+(png|jpg|gif|svg)", {base: base_folder + "/app/images/"})
        .pipe(cache(image()))
        .pipe(gulp.dest("dist/images"));
});
gulp.task("fonts", function () {
    return gulp.src("app/fonts/**/*", {base: base_folder + "/app/fonts/"})
        .pipe(gulp.dest("dist/fonts"));
});
gulp.task("clean:dist", function () {
    return del(["dist/**/*", "!dist/images/**/*"]);
});
gulp.task("clean:dist-end", function () {
    return del(["dist/css", "dist/js"]);
});
gulp.task("modifyUrls", function () {
    return gulp.src("dist/css/*.css", {base: base_folder + "/dist/css"})
        .pipe(modifyCssUrls({
            modify: function (url) {
                var string = url.replace("../", "");

                return string;
            },
            prepend: "",
            append: ""
        }))
        .pipe(gulp.dest("dist/css/"));
});
gulp.task("scripts", function () {
    return gulp.src(
        ["app/js/*.js"], {base: base_folder+"/app/js"})
        .pipe(babel())
        .pipe(gulp.dest("app/js/es5"));
});
gulp.task("inlinesource", function () {
    var options = {};
    return gulp.src("dist/*.html", {base: base_folder + "/dist"})
        .pipe(inlinesource(options))
        .pipe(gulp.dest("dist/"));
});
gulp.task("minify", function () {
    return gulp.src("dist/*.html", {base: base_folder + "/dist"})
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest("dist/"));
});

gulp.task("build",
    gulp.series("clean:dist",
        gulp.parallel("sass", "images", "fonts"),
        "scripts",
        "useref",
        "modifyUrls",
        "inlinesource",
        "minify",
        "clean:dist-end"
    )
);

gulp.task("default",
    gulp.series("sass", "watch"));